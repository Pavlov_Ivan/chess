#ifndef BOARD_H_
#define BOARD_H_

#include <memory>
#include "cell.hpp"
#include "figures.hpp"

namespace chess {

const size_t BOARD_SIZE = 8;

class Board
{
public:
    Board();
    ~Board();
    Figure * getFgByStartPos(uint8_t i, uint8_t j);
    std::shared_ptr<Cell> * operator[](uint8_t i);
    void moveFg(std::shared_ptr<Cell>& from, std::shared_ptr<Cell>& to);
private:
    std::shared_ptr<Cell> cells[BOARD_SIZE][BOARD_SIZE];
};

}

#endif //BOARD_H_
