#include "oxygine-framework.h"
#include <functional>
#include <map>
#include <utility>
#include <exception>
#include "board.hpp"
#include "boardview.hpp"
#include "chess.hpp"

using namespace oxygine;

namespace chess
{

const size_t CELL_SIZE = 50;


BoardView::BoardView()
{   
    Vector2 pos;
    for(size_t i=0; i<BOARD_SIZE; ++i)
    {
        for(size_t j=0; j<BOARD_SIZE; ++j)
        {
            cells[i][j] = new ColorRectSprite;
            cells[i][j]->setSize(CELL_SIZE, CELL_SIZE);
            if(board[i][j]->getColor() == cellColor::BLACK)
                cells[i][j]->setColor(Color::Black);
            else
                cells[i][j]->setColor(Color::Silver);
            pos.x = getStage()->getSize().x / 2 + (int)CELL_SIZE * ((int)j - (int)BOARD_SIZE / 2);
            pos.y = getStage()->getSize().y / 2 - (int)CELL_SIZE * ((int)i - (int)BOARD_SIZE / 2);
            cells[i][j]->setPosition(pos);
                        
            //EventCallback cb = CLOSURE(this, &BoardView::cellClick);
            //cells[i][j]->addEventListener(TouchEvent::CLICK, cb);
            cells[i][j]->addEventListener(TouchEvent::CLICK, 
                    [i, j, this](Event * e)->void
                    {
                        cellClick(i, j);
                    });
            addChild(cells[i][j]);
        }    
    }
    updateFigPos();
}

ResAnim * BoardView::getFigRsrc(fgName name, fgColor cl)
{
    static std::map<fgName, std::string> dic = 
    {
        {fgName::KING,  "king"},
        {fgName::QUEEN, "queen"},
        {fgName::ROOK,  "rook"},
        {fgName::BISHOP,"bishop"},
        {fgName::KNIGHT,"knight"},
        {fgName::PAWN,  "pawn"}
    };

    if(fgColor::BLACK == cl)
        return gameResources.getResAnim(dic[name] + "_black");
    if(fgColor::WHITE == cl)   
        return gameResources.getResAnim(dic[name] + "_white");
    else
        throw std::logic_error("getFigRsrc unknown color");
}

void BoardView::updateFigPos()
{
    for(size_t i=0; i<BOARD_SIZE; ++i)
    {
        for(size_t j=0; j<BOARD_SIZE; ++j)
        {
            if(board[i][j]->isChanged())
            {
                cells[i][j]->removeChildren();
                if(board[i][j]->getFigure() != nullptr)
                {
                    spSprite curFig = new Sprite();
                    curFig->setResAnim(getFigRsrc
                            (board[i][j]->getFigure()->getName(),
                             board[i][j]->getFigure()->getColor()));
                    curFig->setSize(CELL_SIZE, CELL_SIZE);
                    cells[i][j]->addChild(curFig);
                    //figures.push_back(curFig);
                }
            }
        }    
    }
}

void BoardView::cellClick(uint8_t i, uint8_t j)
{
    static bool isFigSelected = false;
    static std::shared_ptr<Cell> from = nullptr;
    static std::shared_ptr<Cell> to = nullptr;
    if(isFigSelected)
    {
        isFigSelected = false;
        to = board[i][j];
        try
        {
            board.moveFg(from, to);
            updateFigPos();
        }
        catch(...)
        {}
    }
    else if(board[i][j]->getFigure() != 0)
    {
        isFigSelected = true;
        from = board[i][j];
    }
};

}
