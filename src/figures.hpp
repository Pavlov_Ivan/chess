#ifndef FIGURES_H_
#define FIGURES_H_

#include <vector>
#include <utility>

namespace chess {

enum class fgColor {BLACK, WHITE};
enum class fgName : size_t {KING, QUEEN, ROOK, BISHOP, KNIGHT, PAWN, COUNT};

class Figure 
{
public:
    Figure(fgColor color, fgName name) : color_(color), name_(name) {}
    virtual bool isStepCorrect
        (const std::pair<uint8_t, uint8_t>& from, const std::pair<uint8_t, uint8_t>& to, 
         const std::vector< std::pair<uint8_t, uint8_t> >& cross) const = 0;
    fgColor getColor() const { return color_; }
    fgName getName() const { return name_; }
private: 
    const fgColor color_;
    const fgName name_;   
};

class King : public Figure
{
public:
    King(fgColor color) : Figure(color, fgName::KING) {}
    bool isStepCorrect
        (const std::pair<uint8_t, uint8_t>& from, const std::pair<uint8_t, uint8_t>& to, 
         const std::vector< std::pair<uint8_t, uint8_t> >& cross) const
    {
        /*Фигура не может знать, всего, что происходит на доске, 
          иначе она будет прибита к доске эта функция проверяет 
          может ли в принципе эта фигура так ходить
          в массиве cross наружу должны возвращаться клетки 
          по которым она идет*/
        return true; 
    }
};

class Queen : public Figure
{
public:
    Queen(fgColor color) : Figure(color, fgName::QUEEN) {}
    bool isStepCorrect
        (const std::pair<uint8_t, uint8_t>& from, const std::pair<uint8_t, uint8_t>& to, 
         const std::vector< std::pair<uint8_t, uint8_t> >& cross) const
    {
        return true; 
    }
};

class Rook : public Figure
{
public:
   Rook(fgColor color) : Figure(color, fgName::ROOK) {}
   bool isStepCorrect
        (const std::pair<uint8_t, uint8_t>& from, const std::pair<uint8_t, uint8_t>& to, 
         const std::vector< std::pair<uint8_t, uint8_t> >& cross) const
    {
        return true; 
    }
};

class Bishop : public Figure
{
public:
   Bishop(fgColor color) : Figure(color, fgName::BISHOP) {}
   bool isStepCorrect
       (const std::pair<uint8_t, uint8_t>& from, const std::pair<uint8_t, uint8_t>& to, 
        const std::vector< std::pair<uint8_t, uint8_t> >& cross) const
    {
        return true; 
    }  
};

class Knight : public Figure
{
public:                                                                
   Knight(fgColor color) : Figure(color, fgName::KNIGHT) {} 
   bool isStepCorrect
       (const std::pair<uint8_t, uint8_t>& from, const std::pair<uint8_t, uint8_t>& to, 
        const std::vector< std::pair<uint8_t, uint8_t> >& cross) const
    {
        return true; 
    }  
};

class Pawn : public Figure
{
public:
    Pawn(fgColor color) : Figure(color, fgName::PAWN) {}
    bool isStepCorrect
        (const std::pair<uint8_t, uint8_t>& from, const std::pair<uint8_t, uint8_t>& to, 
         const std::vector< std::pair<uint8_t, uint8_t> >& cross) const
    {
        return true; 
    }
};


}

#endif //FIGURES_H_

