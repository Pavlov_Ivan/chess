#include <cstddef>
#include <stdexcept>
#include <utility>
#include <memory>
#include "board.hpp"

namespace chess {

Board::Board() {
    for(size_t i=0; i<BOARD_SIZE; ++i)
    {
        for(size_t j=0; j<BOARD_SIZE; ++j)
        {
            if((i+j)%2)
            {
                std::shared_ptr<Cell> tmp
                    (new Cell(i, j, cellColor::WHITE, getFgByStartPos(i, j)));
                cells[i][j] = //std::make_shared<Cell>
                    tmp->getptr();
            }
            else
            {
                std::shared_ptr<Cell> tmp 
                    (new Cell(i, j, cellColor::BLACK, getFgByStartPos(i, j)));
                cells[i][j] =  //std::make_shared<Cell>
                    tmp->getptr();
            }
        }
    }
}

Board::~Board() {
/*    for(size_t i=0; i<BOARD_SIZE; ++i)
    {
        for(size_t j=0; j<BOARD_SIZE; ++j)
        {
            delete cells[i][j];
        }
    }*/
}

Figure * Board::getFgByStartPos(uint8_t i, uint8_t j)
{
    fgColor cl;
    if(i >= BOARD_SIZE || j >= BOARD_SIZE)
        throw std::out_of_range(
                "getFgByStartPos i = " + std::to_string(i) + "j = " + std::to_string(j)
                );
    if(i<=1)
        cl = fgColor::WHITE;
    else if(i>=6)
        cl = fgColor::BLACK;
    else
        return nullptr;
    if(6 == i || 1 == i)
        return new Pawn(cl);
    if(0 == j || 7 == j)
        return new Rook(cl);
    if(1 == j || 6 == j)
        return new Knight(cl);
    if(2 == j || 5 == j)
        return new Bishop(cl);
    if(3 == j)
        return new Queen(cl);
    if(4 == j)
        return new King(cl);
    else
        throw std::logic_error(
                "getFgByStartPos i = " + std::to_string(i) + "j = " + std::to_string(j)
                );
}

std::shared_ptr<Cell> * Board::operator[](uint8_t i)
{
    return cells[i]; 
}

void Board::moveFg(std::shared_ptr<Cell>& from, std::shared_ptr<Cell>& to)
{
    static std::vector< std::pair<uint8_t, uint8_t> > cross;
    cross.reserve(BOARD_SIZE);
    if(from->getFigure() != nullptr && to->getFigure() != nullptr &&
            from->getFigure()->getColor() == to->getFigure()->getColor())
    {
        throw std::runtime_error("moveFg can't kill our figures");
    }
    if(from->getFigure()->isStepCorrect
            (std::make_pair(from->getX(), from->getY()), 
             std::make_pair(to->getX(), to->getY()), cross))
    {
        /*Тут все остальные проверки в том числе и cross - не идем ли свозь фигуры*/    
    }
    else
    {
        throw std::runtime_error("moveFg incorrect step");
    }
    to->moveFigHere(from);
}

}
