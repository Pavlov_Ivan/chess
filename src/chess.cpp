#include "boardview.hpp"
#include "chess.hpp"

using namespace chess;
using namespace oxygine;

//it is our resources
//in real project you would have more than one Resources declarations.
//It is important on mobile devices with limited memory and you would load/unload them
Resources gameResources;

void chess_preinit() {}

//called from main.cpp
void chess_init()
{
    //load xml file with resources definition
    gameResources.loadXML("res.xml");
    spBoardView actor = new BoardView;
    getStage()->addChild(actor);
}


//called each frame from main.cpp
void chess_update()
{
}

//called each frame from main.cpp
void chess_destroy()
{
    //free previously loaded resources
    gameResources.free();
}
