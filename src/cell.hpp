#ifndef CELL_H_
#define CELL_H_

#include<cstdint>
#include <memory>
#include "figures.hpp"

namespace chess {

enum class cellColor {BLACK, WHITE};

class Figure;

class Cell : public std::enable_shared_from_this<Cell> {
public:
    Cell(uint8_t x, uint8_t y, cellColor color, Figure *figure = nullptr) 
            :  x_(x), y_(y), color_(color), figure_(figure), isChanged_(true) {}
    std::shared_ptr<Cell> getptr() {
        return shared_from_this();
    }
    const Figure *getFigure() const { isChanged_ = false; return figure_; }
    void setFigure(const Figure * figure) { 
        if(figure != figure_)
        {
            if(nullptr != figure_)
                delete figure_;
            figure_ = figure;
            isChanged_ = true;
        }
    }
    ~Cell()
    {
        if(figure_ != nullptr)
            delete figure_;
    }
    void moveFigHere(std::shared_ptr<Cell>& from)
    {
        if(!(this->getX() == from->getX() && this->getY() == from->getY()))
        {
            setFigure(from->getFigure());
            from->figure_ = nullptr;
            from->isChanged_ = true;
        }
    }
    bool isChanged() const 
    { 
        bool tmp = isChanged_;         
        //ИЗМЕНЕНИЯ С МОМЕНТА ПОСЛЕДНЕГО ВЫЗОВА isChanged/getFigure!!!
        isChanged_ = false;
        return tmp;
    }
    cellColor getColor(){ return color_; }
    uint8_t getX() const { return x_; }
    uint8_t getY() const { return y_; }
private:
    const Figure *figure_;
    const cellColor color_;
    const uint8_t x_;
    const uint8_t y_;
    mutable bool isChanged_;
};

}

#endif //CELL_H_
