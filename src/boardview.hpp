#ifndef BOARDVIEW_H_
#define BOARDVIEW_H_

#include "oxygine-framework.h"
#include "board.hpp"

namespace chess
{

class BoardView: public oxygine::Actor
{
public:
    BoardView();
    void updateFigPos();
    void cellClick(uint8_t i, uint8_t j);
private:
    oxygine::spSprite cells[BOARD_SIZE][BOARD_SIZE];
    Board board;
    oxygine::ResAnim * getFigRsrc(fgName, fgColor);

};

typedef oxygine::intrusive_ptr<BoardView> spBoardView;


}

#endif //BOARDVIEW_H_
