#ifndef CHESS_H_
#define CHESS_H_

#include "oxygine-framework.h"

void chess_preinit();
void chess_init();
void chess_destroy();
void chess_update();

extern oxygine::Resources gameResources;

#endif //CHESS_H_
